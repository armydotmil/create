$(document).ready(function(){
	$('#pageNav li a').click(function(){
	$('#pageNav li a').removeClass("current");
		var thisLink = $(this);
		thisLink.addClass("current");
		var div = thisLink.attr("href");				
		$("#tabContainer .item").removeClass("active");
		$(div).addClass("active");
		return false;
	});
	$('#cycle').cycle({ 
	fx: 'fade', 
	speed: 200, 
	timeout: 50000, 
	cleartype:1, // enable cleartype corrections
	cleartypeNoBg: true  // fixes background color change
	});
});
